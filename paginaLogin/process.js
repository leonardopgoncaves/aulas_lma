const {createApp} = Vue;

createApp({
    data(){
        return{
            username: "",
            password: "",
            error: null,
            correto: null,
            sucesso: null,

            //arrays para armazenamento dos usuarios e senhas
            usuarios: ["admin", "adriano"],
            senhas: ["1234", "1234"],

            //variaveis para armazenamento do usuario e senha que sera cadastrado
            newUsername: "",
            newPassword: "",
            confirmPassword: "",

                mostrarEntrada: false,

                mostrarLista: false,
        }// Fechamento return
    }, // Fechamento data

    methods:{
        login(){
            setTimeout(() => {
                if((this.username === 'Adriano' && this.password === '12342023') || (this.username === 'Vinicius' && this.password === '1234567890')){
                    this.correto = "Login Realizado com Sucesso!";
                    this.error = null;
                }
                else{
                    this.error = "Nome ou senha incorretos!"
                    this.correto = null;
                }
            },);
        },//Fechamento login

        loginArray(){
            if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }//Fechamento if
            
            this.mostrarEntrada = false
            setTimeout(() => {
                this.mostrarEntrada = true;
                const index = this.usuarios.indexOf(this.username);
                if(index !== -1 && this.senhas[index] === this.password){
                    this.error = null;
                    this.sucesso = "Login efetuado com sucesso!";

                    localStorage.setItem("username", this.username);
                    localStorage.setItem("password", this.password);
                }
                else{
                    this.sucesso = null;
                    this.error = "Usuário ou senha incorrretos!";
                }
                this.username = "";
                this.password = "";
            }, 500);
        },//Fechamento loginArray

        adicionarUsuario(){
            this.mostrarEntrada = false;

            //updete no conteúdo dos arrays para recuperar os dados armazenados
            if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }//Fechamento if

            setTimeout(() => {
                this.mostrarEntrada = true;
                if(!this.usuarios.includes(this.newUsername) && this.newUsername !== ""){
                    if(this.newPassword && this.newPassword === this.confirmPassword){
                        this.usuarios.push(this.newUsername);
                        this.senhas.push(this.newPassword);
                        
                        //Atualizando os valores dos arrays no armazenamento local
                        localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                        localStorage.setItem("senhas", JSON.stringify(this.senhas));
                        
                        this.newUsername = "";
                        this.newPassword = "";
                        this.confirmPassword = "";
                        this.sucesso = "Usúario cadastrado com sucesso!";
                        this.error = null;
                    }
                    else{
                        this.error = "Por favor, digite uma senha válida!";
                    }
                }//Fechamento if
                else{
                    this.error = "Por favor, digite uma usuário válida!";
                    this.sucesso = null;
                }//Fechamento else
            }, 500);
        },//Fechamento adicionandoUsuario

        verCadastrados(){
            if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }//Fechamento if
            this.mostrarLista = !this.mostrarLista;
        },//Fechamento verCadastrados

        paginaCadastro(){
            this.mostrarEntrada = false
            setTimeout(() => {
                this.mostrarEntrada = true
                this.sucesso = "Carregando página de cadastro!";
            }, 500);
        

            setTimeout(() => {
                window.location.href = "cadastro.html"

            }, 1000);

        },

    },//Fechamento methods
}).mount("#app"); // Fechamento createApp