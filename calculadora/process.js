const {createApp} = Vue;
createApp
({
    data()
    {
        return{
            display:"0",
            operandoAtual:null,
            operandoAnterior:null,
            operador:null,
            };//fechamento do return
    },//fechamento da função "data"

    methods:{
        handleButtonClick(botao){
            switch(botao){
                case"/":
                case"-":
                case"+":
                case"*":
                    this.handleOperation(botao);
                    break;

                case".":
                    this.handleDecimal();
                    break;

                case"=":
                    this.handleEquals();
                    break

                default:
                    this.handleNumber(botao);
                    break;
            }
        },

        handleNumber(numero){
            if(this.display === "0")
            {
                this.display = numero.toString();
            }
            else
            {
                this.display += numero.toString();
            }
        },

        handleOperation(operacao){
            if(this.operandoAtual !== null){
                this.handleEquals();
            }//fechamento do if
            this.operador = operacao;

            this.operandoAtual = parseFloat(this.display);
            this.display = "0";
        },

        handleEquals(){
            const displayAtual = parseFloat(this.display);
            if(this.operandoAtual !== null && this.operador !== null){
                switch(this.operador){
                    case "+":
                        this.display = (this.operandoAtual + displayAtual).toString();
                        break;
                    case "-":
                        this.display = (this.operandoAtual - displayAtual).toString();
                        break;
                    case "*":
                        this.display = (this.operandoAtual * displayAtual).toString();
                        break;
                    case "/":
                        this.display = (this.operandoAtual / displayAtual). toString();
                        break;
                    
                }//fechamento do switch

                this.operandoAnterior = this.operandoAtual;
                this.operandoAtual = null;
                this.operador = null;

            }//fechamento do if
            else
            {
                this.operandoAnterior = displayAtual;
            }//fechamento do else

        },//fechamento handleEquals

        handleDecimal(){
            if(!this.display.includes("."))
            {
                this.display += ".";
            }//fechamento do if
        },//fechamento do handleDecimal

        handleClear(){
            this.display = "0";
            this.operandoAtual = null;
            this.operandoAnterior = null;
            this.operador = null;
        },//fim  do handleClear

        handleClearAtual(){
            this.display = "0";
            this.operandoAnterior = null;
        },

        handleBackSpace(){
            this.backspace
            
        },

    },

}).mount("#app");//fechamento do "createApp"