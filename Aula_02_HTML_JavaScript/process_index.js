//processamenento dos dados de formulario "index.html"

//acessondo os elementos formulario do html
const formulario = document.getElementById("Formulario1");

//adiciona um ouvinte de eventos a um elemento html
formulario.addEventListener("submit", function(evento1)
{
    evento1.preventDefault();/*previne o comportamento padrão de um elemento HTML em resposta a um evento*/

    //constantes para tratar as dados recebidas dos elementos do formulario
    const nome = document.getElementById("nome").value;
    const email = document.getElementById("email").value;

    //exibe um alerta com os daods coletados
    alert(`Nome: ${nome} ---- E-mail: ${email}`);

    const updateResultado = document.getElementById("resultado");

    updateResultado.value = `Nome: ${nome} ---- E-mail: ${email}`;

    updateResultado.style.width = updateResultado.scrollWidth + "px";
});