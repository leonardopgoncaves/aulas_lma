const {createApp} = Vue;
createApp({
    data(){
        return {
            randomIndex: 0,
            randomIndexInternet: 0,

            //vetor de imagens locais
            imagensLocais:[
                './imagens/lua.jpg',
                './imagens/sol.jpg',
                './imagens/SENAI_logo.png',
            ],

            imagensInternet:[
                'https://img.freepik.com/vetores-gratis/bela-casa_24877-50819.jpg',
                'https://i.ytimg.com/vi/zHqmNcSMNTU/maxresdefault.jpg',
                'https://static.wikia.nocookie.net/joke-battles/images/6/6a/Drip_Goku_HD.png'
            ],

        };//Fim return
    },//Fim data

    computed:{
        randomImage()
        {
            return this.imagensLocais[this.randomIndex];
        },

        randomImageInternet()
        {
            return this.imagensInternet[this.randomIndexInternet];
        }
    },

    methods:{
        getRandomImagem()
        {
            this.randomIndex=Math.floor(Math.random() * this.imagensLocais.length);

            this.randomIndexInternet=Math.floor(Math.random() * this.imagensInternet.length);
        }
    },

}).mount("#app");