const {createApp} = Vue;

createApp({
    data(){
        return{
            show: false,
            itens: ["Bola", "Bolsa", "Tenis"],

        };
    },

    methods: {
        showItens: function(){
            this.show = !this.show;
        }
    },

}).mount("#app");